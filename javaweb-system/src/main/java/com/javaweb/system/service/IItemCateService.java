// +----------------------------------------------------------------------
// | JavaWeb_Vue_Pro前后端分离旗舰版框架 [ JavaWeb ]
// +----------------------------------------------------------------------
// | 版权所有 2019~2020 南京JavaWeb研发中心
// +----------------------------------------------------------------------
// | 官方网站: http://www.javaweb.vip/
// +----------------------------------------------------------------------
// | 作者: 鲲鹏 <1175401194@qq.com>
// +----------------------------------------------------------------------

package com.javaweb.system.service;

import com.javaweb.common.utils.JsonResult;
import com.javaweb.system.common.IBaseService;
import com.javaweb.system.entity.ItemCate;

/**
 * <p>
 * 栏目管理表 服务类
 * </p>
 *
 * @author 鲲鹏
 * @since 2020-11-07
 */
public interface IItemCateService extends IBaseService<ItemCate> {

    /**
     * 获取栏目列表
     *
     * @return
     */
    JsonResult getCateList();

    /**
     * 获取栏目名称
     *
     * @param cateId    栏目ID
     * @param delimiter 分隔符
     * @return
     */
    String getCateName(Integer cateId, String delimiter);

}
