<template>
  <div class="ele-body">
    <a-card :bordered="false">
      <!-- 搜索表单 -->
      <a-form
          :model="where"
          :label-col="{md: {span: 6}, sm: {span: 24}}"
          :wrapper-col="{md: {span: 18}, sm: {span: 24}}">
        <a-row>
      <#if model_column?exists>
        <#list model_column as model>
          <#if model.columnName = 'name' || model.columnName = 'title'>

          <a-col :lg="6" :md="12" :sm="24" :xs="24">
            <a-form-item label="${model.columnComment}:">
              <a-input
                v-model:value.trim="where.${model.columnName}"
                placeholder="请输入${model.columnComment}"
                allow-clear/>
            </a-form-item>
          </a-col>
          </#if>
          <#if model.hasColumnCommentValue = true>
          <!-- ${model.columnCommentName}下拉单选 -->
          <a-col :lg="6" :md="12" :sm="24" :xs="24">
            <a-form-item label="${model.columnCommentName}:">
              <a-select
                v-model:value="where.${model.columnName}"
                placeholder="请选择${model.columnCommentName}"
                allow-clear>
              <#if model.columnCommentValue?exists>
                <#list model.columnCommentValue?keys as key>
                <a-select-option :value="${key}">${model.columnCommentValue[key]}</a-select-option>
                </#list>
              </#if>
              </a-select>
            </a-form-item>
          </a-col>
          </#if>
        </#list>
      </#if>
          <a-col :lg="6" :md="12" :sm="24" :xs="24">
            <a-form-item style="padding-left:10px;" :wrapper-col="{span: 24}">
              <a-space>
                <a-button type="primary" @click="reload">查询</a-button>
                <a-button @click="reset">重置</a-button>
              </a-space>
            </a-form-item>
          </a-col>
        </a-row>
      </a-form>
      <!-- 表格 -->
      <ele-pro-table
        ref="table"
        row-key="id"
        :datasource="url"
        :columns="columns"
        :where="where"
        v-model:selection="selection"
        :scroll="{x: 'max-content'}">
        <template #toolbar>
          <a-space>
            <a-button type="primary" @click="openEdit()" v-if="permission.includes('sys:${entityName?lower_case}:add')">
              <template #icon>
                <plus-outlined/>
              </template>
              <span>新建</span>
            </a-button>
            <a-button type="danger" @click="removeBatch" v-if="permission.includes('sys:${entityName?lower_case}:dall')">
              <template #icon>
                <delete-outlined/>
              </template>
              <span>删除</span>
            </a-button>
          </a-space>
        </template>
  <#if model_column?exists>
    <#list model_column as model>
      <#if model.changeColumnName?uncap_first != 'createUser' && model.changeColumnName?uncap_first != 'updateUser' && model.changeColumnName?uncap_first != 'mark'>
        <#if model.hasColumnCommentValue = true>
          <#if model.columnSwitch == true>
        <template #${model.changeColumnName?uncap_first}="{ text, record }">
          <a-switch
            :checked="text===1"
            @change="(checked) => set${model.changeColumnName?cap_first}(checked, record)"/>
        </template>
          <#else>
        <template #${model.changeColumnName?uncap_first}="{ record }">
          <a-tag :color="['green', 'red', 'blue', 'orange', ''][record.${model.changeColumnName?uncap_first}-1]">
            {{[${model.columnValue}][record.${model.changeColumnName?uncap_first}-1] }}
          </a-tag>
        </template>
          </#if>
        <#elseif model.columnImage == true>
        <template #${model.changeColumnName?uncap_first}="{ record }">
          <a-image :width="35" :src="record.${model.changeColumnName?uncap_first}" />
        </template>
        <#else>

        </#if>
      </#if>
    </#list>
  </#if>
        <template #action="{ record }">
          <a-space>
            <a @click="openEdit(record)" v-if="permission.includes('sys:${entityName?lower_case}:edit')">修改</a>
            <a-divider type="vertical"/>
            <a-popconfirm
              title="确定要删除此${tableAnnotation}吗？"
              @confirm="remove(record)">
              <a class="ele-text-danger" v-if="permission.includes('sys:${entityName?lower_case}:delete')">删除</a>
            </a-popconfirm>
          </a-space>
        </template>
      </ele-pro-table>
    </a-card>
  </div>
  <!-- 编辑弹窗 -->
  <${entityName?lower_case}-edit
    v-model:visible="showEdit"
    :data="current"
    @done="reload"/>
</template>

<script>
  import { mapGetters } from "vuex";
  import {createVNode} from 'vue'
  import {
    PlusOutlined,
    DeleteOutlined,
    ExclamationCircleOutlined
  } from '@ant-design/icons-vue';
  import ${entityName}Edit from './${entityName?lower_case}-edit';

  export default {
    name: 'System${entityName}',
    components: {
      PlusOutlined,
      DeleteOutlined,
      ${entityName}Edit
    },
    computed: {
      ...mapGetters(["permission"]),
    },
    data() {
      return {
        // 表格数据接口
        url: '/${entityName?lower_case}/index',
        // 表格列配置
        columns: [
          {
            key: 'index',
            title: '编号',
            width: 48,
            align: 'center',
            fixed: 'left',
            customRender: ({index}) => this.$refs.table.tableIndex + index
          },
    <#if model_column?exists>
      <#list model_column as model>
        <#if model.changeColumnName?uncap_first != 'createUser' && model.changeColumnName?uncap_first != 'updateUser' && model.changeColumnName?uncap_first != 'mark'>
          <#if (model.columnType = 'DATETIME' || model.columnType = 'DATE' || model.columnType = 'TIME' || model.columnType = 'YEAR' || model.columnType = 'TIMESTAMP')>
          {
            title: '${model.columnComment}',
            dataIndex: '${model.changeColumnName?uncap_first}',
            width:120,
            align: 'center',
            customRender: ({text}) => this.$util.toDateString(text)
          },
          <#elseif model.hasColumnCommentValue = true>
          <#if model.columnSwitch == true>
          {
            title: '${model.columnCommentName}',
            dataIndex: '${model.changeColumnName?uncap_first}',
            width:120,
            align: 'center',
            slots: {customRender: '${model.changeColumnName?uncap_first}'}
          },
          <#else>
          {
            title: '${model.columnCommentName}',
            dataIndex: '${model.changeColumnName?uncap_first}',
            align: 'center',
            width:120,
            slots: {customRender: '${model.changeColumnName?uncap_first}'}
          },
          </#if>
          <#elseif model.columnImage == true>
          {
            title: '${model.columnComment}',
            dataIndex: '${model.changeColumnName?uncap_first}',
            align: 'center',
            width:100,
            slots: {customRender: '${model.changeColumnName?uncap_first}'}
          },
          <#else>
          {
            title: '${model.columnComment}',
            dataIndex: '${model.changeColumnName?uncap_first}',
            width:120,
            align: 'center'
          },
          </#if>
        </#if>
      </#list>
    </#if>
          {
            title: '操作',
            key: 'action',
            width: 100,
            align: 'center',
            fixed: 'right',
            slots: {customRender: 'action'}
          }
        ],
        // 表格搜索条件
        where: {},
        // 表格选中数据
        selection: [],
        // 当前编辑数据
        current: null,
        // 是否显示编辑弹窗
        showEdit: false
      };
    },
    methods: {
      /* 搜索 */
      reload() {
        this.selection = [];
        this.$refs.table.reload({page: 1});
      },
      /*  重置搜索 */
      reset() {
        this.where = {};
        this.$nextTick(() => {
          this.reload();
        });
      },
      /* 删除单个 */
      remove(row) {
        const hide = this.$message.loading('请求中..', 0);
        this.$http.delete('/${entityName?lower_case}/delete/' + [row.id]).then(res => {
          hide();
          if (res.data.code === 0) {
            this.$message.success(res.data.msg);
            this.reload();
          } else {
            this.$message.error(res.data.msg);
          }
        }).catch(e => {
          hide();
          this.$message.error(e.message);
        });
      },
      /* 批量删除 */
      removeBatch() {
        if (!this.selection.length) {
          this.$message.error('请至少选择一条数据');
          return;
        }
        this.$confirm({
          title: '提示',
          content: '确定要删除选中的${tableAnnotation}吗?',
          icon: createVNode(ExclamationCircleOutlined),
          maskClosable: true,
          onOk: () => {
            const hide = this.$message.loading('请求中..', 0);
            this.$http.delete('/${entityName?lower_case}/delete', {
              data: this.selection.map(d => d.id)
            }).then(res => {
              hide();
              if (res.data.code === 0) {
                this.$message.success(res.data.msg);
                this.reload();
              } else {
                this.$message.error(res.data.msg);
              }
            }).catch(e => {
              hide();
              this.$message.error(e.message);
            });
          }
        });
      },
      /* 打开编辑弹窗 */
      openEdit(row) {
        this.current = row;
        this.showEdit = true;
      },
<#if model_column?exists>
  <#list model_column as model>
    <#if model.columnSwitch == true>
      /* 修改状态 */
      set${model.changeColumnName?cap_first}(checked, row) {
        let params = Object.assign({}, {
          id : row.id,
          ${model.changeColumnName?uncap_first} : checked ? 1 : 2,
        });
        this.$http.put('/${entityName?lower_case}/set${model.changeColumnName?cap_first}', params).then(res => {
          if (res.data.code === 0) {
            row.${model.changeColumnName?uncap_first} = checked ? 1 : 2;
            this.$message.success(res.data.msg);
          } else {
            this.$message.error(res.data.msg);
          }
        }).catch(e => {
          this.$message.error(e.message);
        });
      },
    </#if>
  </#list>
</#if>
    }
  }
</script>

<style scoped>
</style>
