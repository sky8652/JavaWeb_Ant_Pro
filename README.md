
<p align="center">
    <img src="https://images.gitee.com/uploads/images/2021/0518/201751_1abceab0_478496.png" width="200" height="200" />
</p>

<h1 align="center"> 为梦想而创作：JavaWeb基于AntDesign+Vue前后端分离后台开发框架</h1>

### 项目介绍
JavaWeb_Ant_Pro 是基于 SpringBoot2+Vue+AntDesign+Shiro+MybatisPlus 研发的权限(RBAC)及内容管理系统，致力于做更简洁的后台管理框架，包含系统管理、代码生成、权限管理、站点、广告、布局、字段、配置等一系列常用的模块，整套系统一键生成所有模块（包括前端UI），一键实现CRUD，简化了传统手动抒写重复性代码的工作。 同时，框架提供长大量常规组件，如上传单图、上传多图、上传文件、下拉选择、复选框按钮、单选按钮，城市选择、富文本编辑器、权限颗粒度控制等高频使用的组件，代码简介，使用方便，节省了大量重复性的劳动，降低了开发成本，提高了整体开发效率，整体开发效率提交80%以上，JavaWeb框架专注于为中小企业提供最佳的行业基础后台框架解决方案，执行效率、扩展性、稳定性值得信赖，操作体验流畅，使用非常优化，欢迎大家使用及进行二次开发。
* 模块化：全新的架构和模块化的开发机制，便于灵活扩展和二次开发。
* 模型/栏目/分类信息体系：通过栏目和模型绑定，以及不同的模型类型，不同栏目可以实现差异化的功能，轻松实现诸如资讯、下载、讨论和图片等功能。通过分类信息和栏目绑定，可以自动建立索引表，轻松实现复杂的信息检索。
* 支持SQLServer、MySQL、Oracle、PostgreSQL、SQLite等多数据库类型。模块化设计，层次结构清晰。  
* AUTH权限认证，操作权限控制精密细致，对所有管理链接都进行权限验证，可控制到导航菜单、功能按钮。提高开发效率及质量。
* 常用类封装，日志、缓存、验证、字典、文件（本地、七牛云）。等等，目前兼容浏览器（Chrome、Firefox、360浏览器等）
* 适用范围：可以开发OA、ERP、BPM、CRM、WMS、TMS、MIS、BI、电商平台后台、物流管理系统、快递管理系统、教务管理系统等各类管理软件。

### 功能特性
- **严谨规范：** 提供一套有利于团队协作的结构设计、编码、数据等规范。
- **高效灵活：** 清晰的分层设计、钩子行为扩展机制，解耦设计更能灵活应对需求变更。
- **严谨安全：** 清晰的系统执行流程，严谨的异常检测和安全机制，详细的日志统计，为系统保驾护航。
- **组件化：** 完善的组件化设计，丰富的表单组件，让开发列表和表单更得心应手。无需前端开发，省时省力。
- **简单上手快：** 结构清晰、代码规范、在开发快速的同时还兼顾性能的极致追求。
- **自身特色：** 权限管理、组件丰富、第三方应用多、分层解耦化设计和先进的设计思想。


## 开发者信息
* 系统名称：JavaWeb_Ant_Pro权限(RBAC)及内容管理框架  
* 作者：鲲鹏
* 作者QQ：1175401194  
* 官网网址：[http://www.javaweb.vip/](http://www.javaweb.vip/)  
* 文档网址：[http://docs.ant.pro.javaweb.vip](http://docs.ant.pro.javaweb.vip)

## 后台演示（用户名:admin 密码:123456）
- 演示地址：[http://manage.ant.pro.javaweb.vip](http://manage.ant.pro.javaweb.vip)
- 登录用户名：admin
- 登录密码：123456
- 验证码：520

## 技术支持

[技术支持QQ：1175401194](http://wpa.qq.com/msgrd?v=3&amp;uin=1175401194&amp;site=qq&amp;menu=yes)


### 版本说明

| 版本名称 | 说明 | 地址 |
| :---: | :---: | :---: |
| JavaWeb 混编专业版 | 采用SpringBoot2+Thymeleaf+layui | https://gitee.com/javaweb520/JavaWeb |
| JavaWeb_Pro 混编旗舰版 | 采用SpringBoot2+Thymeleaf+layui | https://gitee.com/javaweb520/JavaWeb_Pro |
| JavaWeb_Vue专业版 | 前后端分离版，采用SpringBoot2+Vue+ElementUI | https://gitee.com/javaweb520/JavaWeb_Vue |
| JavaWeb_Vue_Pro旗舰版 | 采用SpringBoot2+Vue+ElementUI | https://gitee.com/javaweb520/JavaWeb_Vue_Pro |
| JavaWeb_Ant_Pro旗舰版 | 采用SpringBoot2+Vue+AntDesign | https://gitee.com/javaweb520/JavaWeb_Ant_Pro |
| JavaWeb_Cloud微服务专业版 | 采用SpringCloud+Vue+ElementUI | https://gitee.com/javaweb520/JavaWeb_Cloud |
| JavaWeb_Cloud_Pro微服务旗舰版 | 采用SpringCloud+Vue+ElementUI | https://gitee.com/javaweb520/JavaWeb_Cloud_Pro |
| JavaWeb_Cloud_Ant微服务旗舰版 | 采用SpringCloud+Vue+AntDesign | https://gitee.com/javaweb520/JavaWeb_Cloud_Ant |

## 效果图展示

#### 效果图1
 ![效果图1](./uploads/demo/1.png)

#### 效果图2
 ![效果图2](./uploads/demo/2.png)
 
#### 效果图3
 ![效果图3](./uploads/demo/3.png)
 
#### 效果图4
 ![效果图4](./uploads/demo/4.png)
 
#### 效果图5
 ![效果图5](./uploads/demo/5.png)
 
#### 效果图6
 ![效果图5](./uploads/demo/6.png)
 
#### 效果图7
 ![效果图7](./uploads/demo/7.png)
 
#### 效果图8
 ![效果图8](./uploads/demo/8.png)
 
#### 效果图9
 ![效果图9](./uploads/demo/9.png)
 
#### 效果图10
 ![效果图10](./uploads/demo/10.png)
 
#### 效果图11
 ![效果图11](./uploads/demo/11.png)
 
#### 效果图12
 ![效果图12](./uploads/demo/12.png)
 
#### 效果图13
![效果图13](./uploads/demo/13.png)

#### 效果图14
![效果图14](./uploads/demo/14.png)

#### 效果图15
![效果图15](./uploads/demo/15.png)

#### 效果图16
![效果图16](./uploads/demo/16.png)

#### 效果图17
![效果图17](./uploads/demo/17.png)

#### 效果图18
![效果图18](./uploads/demo/18.png)

#### 效果图19
![效果图19](./uploads/demo/19.png)


## 安全&缺陷
如果你发现了一个安全漏洞或缺陷，请发送邮件到 1175401194@qq.com,所有的安全漏洞都将及时得到解决。


## 鸣谢
感谢[MybatisPlus](https://mp.baomidou.com/)、[AntDesign](https://2x.antdv.com/components/transfer-cn)、[Vue](https://cn.vuejs.org/)等优秀开源项目。

## 版权信息

JavaWeb商业版使用需授权，未授权禁止恶意传播和用于商业用途，否则将追究相关人的法律责任。

本项目包含的第三方源码和二进制文件之版权信息另行标注。

版权所有Copyright © 2017~2021 javaweb.vip (http://www.javaweb.vip)

All rights reserved。

更多细节参阅 [LICENSE.txt](LICENSE.txt)
